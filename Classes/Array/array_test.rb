require "minitest/spec"
require "minitest/autorun"
require "minitest/reporters"
Minitest::Reporters.use!

class TestArray < Minitest::Unit::TestCase

  def setup
    @array = [1,2,3,4,5,6,7,8,9,10]
  end

  # Public Class Methods

  def test_new_array_has_zero_size
    assert(Array.new.size == 0)
  end

  def test_new_array_can_contain_different_object_types
    assert_equal([1, 3, true, false, 'computer'], Array.new << 1 << 3 << true << false << 'computer')
  end

  def test_array_new_can_be_called_with_one_arg
    assert_equal([nil, nil, nil], Array.new(3))
  end

  def test_array_new_can_be_called_with_two_args
    assert_equal(['howdy', 'howdy', 'howdy'], Array.new(3, 'howdy'))
  end

  def test_array_access_via_brackets_method
    array = ['thirty', '1', 'thousand']
    assert_equal('1', array[1])
  end

  def test_array_access_via_at_method
    array = ['thirty', '1', 'thousand']
    assert_equal('thirty', array.at(0))    
  end

  def test_array_access_fetch_method
    assert_equal(9, @array.fetch(-2))
  end

  def test_array_first_returns_indicie_zero
    assert_equal(1, @array.first)
  end

  def test_array_take_returns_first_5_elements_in_itself
    assert_equal([1, 2, 3, 4, 5], @array.take(5))    
  end

  def test_array_drop_returns_remaining_elements_after_n_has_been_dropped
    assert_equal([6,7,8,9,10], @array.drop(5))    
  end

  def test_array_keeps_track_of_length
    assert_equal(10, @array.length)
  end

  def test_array_contains_elements
    empty_array = []
    assert_equal(false, @array.empty?)
    assert_equal(true, empty_array.empty?)
  end

  def test_array_contains_specific_item
    assert_equal(true, @array.include?(7))
    assert_equal(false, @array.include?(11))
  end

  def test_array_can_append_with_push
    assert_equal([1,2,3,4,5,6,7,8,9,10,11], @array.push(11))
  end

  def test_array_can_append_with_shovel
    assert_equal([1,2,3,4,5,6,7,8,9,10,11], @array << 11)
  end

  def test_array_can_add_element_to_beginning_its_beginning
    assert_equal([0,1,2,3,4,5,6,7,8,9,10], @array.unshift(0))
  end

  def test_array_can_add_element_at_any_position
    assert_equal([1,2,3,4,5,'ice cold water',6,7,8,9,10], @array.insert(5, 'ice cold water'))
  end

  def test_array_can_add_multiple_elements_at_one_time
    assert_equal([1,2,3,4,5,'ice cold water', 'automobile', 'kittens',6,7,8,9,10], @array.insert(5, 'ice cold water', 'automobile', 'kittens'))
  end

  def test_array_removes_last_element_and_returns_it
    assert_equal(10, @array.pop)
  end

  def test_array_retreives_and_removes_first_element
    assert_equal(1, @array.shift)
  end

  def test_array_deletes_element_anywhere
    assert_equal(3, @array.delete_at(2))
  end

  def test_array_removes_all_nil_values_at_once
    array = ['I', nil, 'am', nil, 'having', nil, 'fun', nil, '!']
    assert_equal(['I', 'am', 'having', 'fun', '!'], array.compact)
    assert_equal(['I', 'am', 'having', 'fun', '!'], array.compact!)
  end

  def test_array_removes_duplicate_elements_but_non_destructive
    array = [2,4,2,8,2,4,5,3,6,7,8,8,9,9,9,8,10]
    assert_equal([2,4,8,5,3,6,7,9,10], array.uniq)
    assert_equal([2,4,8,5,3,6,7,9,10], array.uniq!)
  end

  def test_array_elements_can_be_iterated_over_and_how
    new_array = []
    @array.each { |num| new_array << num += 100 }
    assert_equal([101,102,103,104,105,106,107,108,109,110], new_array)
  end

  def test_array_elements_can_iterate_in_reverse_order
    words = %w[first second third fourth fifth]
    string = ""
    words.reverse_each { |word| string += "#{word}" } 
    assert_equal("fifthfourththirdsecondfirst", string)   
  end

  def test_array_map_will_create_new_array_based_on_original
    new_array = @array.map! { |num| num * 2 }
    assert_equal([2,4,6,8,10,12,14,16,18,20], new_array)
  end  

  def test_array_selects_elements_based_on_block_criteria
    new_array = @array.select { |a| a > 5 }
    assert_equal([6,7,8,9,10], new_array)
  end

  def test_array_rejects_elements_based_on_block_criteria
    new_array = @array.select { |a| a < 5 }
    assert_equal([1,2,3,4], new_array)
  end

  def test_array_will_drop_elements_while_block_conditions_are_true
    new_array = @array.drop_while { |a| a < 5 }
    assert_equal([5,6,7,8,9,10], new_array)
  end

  def test_array_will_delete_element_if_block_conditions_are_true
    @array.delete_if { |a| a % 2 == 0 }
    assert_equal([1,3,5,7,9], @array) 
  end

  def test_array_will_keep_element_if_block_conditions_are_true
    @array.keep_if { |a| a % 3 == 0 }
    assert_equal([3,6,9], @array)
  end

  def test_array_will_populate_with_given_objects
    array = Array.[](1, 'a', "chimichangas")
    assert_equal([1, 'a', "chimichangas"], array)
  end

  def test_array_new_will_return_a_new_array_with_size_and_object_args
    array = Array.new(size=5, obj=nil)
    assert_equal([nil,nil,nil,nil,nil], array)
  end

  def test_second_array_new_will_return_elements_from_first_array
    first_array = ["Ten", "Thousand"]
    second_array = Array.new(first_array)
    assert_equal(["Ten","Thousand"], second_array)
  end

  def test_if_array_class_method_argument_is_an_array
    is_array = Array.try_convert([1])
    is_not_array = Array.try_convert('1')
    assert_equal(true, is_array.kind_of?(Array))
    assert_equal(false, is_not_array.kind_of?(Array))
  end

  # Public Instance Methods

  def test_new_array_from_two_arrays_containing_common_elements
    new_array = [1,2,3,5,6,7] & [1,6, 13, 14, 15]
    assert_equal([1,6], new_array)
  end

  def test_array_repetition_with_string_argument_is_equivalnet_to_join_method
    new_array = [1,2,3] * ", "
    assert_equal("1, 2, 3", new_array)
  end

  def test_array_repetition_with_non_string_argument_builds_new_array_by_cncatenation_the_int_copies_of_self
    new_array = [1,2,3] * 3
    goal = [1,2,3,1,2,3,1,2,3]
    assert_equal(goal, new_array)
  end

  def test_concatenation_of_two_arrays_produce_a_third_array
    array = [1,2,3] + [4,5]
    assert_equal([1,2,3,4,5], array)
  end

  def test_difference_of_two_arrays_returns_a_copy_array_without_duplicate_elements
    array = [1,1,2,3,3,3,4,5,6,6,7,8,9,10] - [1,3,5,7,9]
    assert_equal([2,4,6,6,8,10], array)
  end

  def test_shovel_appends_given_object_to_the_end_of_an_array
    array = [1,2] << "c" << "d" << [26, 28]
    assert_equal([1,2,"c","d",[26,28]], array)
  end

  def test_array_comparison_operator_returns_if_other_array_is_less_than_equal_to_or_greater_than_primary_array
    array = [1,2,3,4,5,6]
    other_array = [2,4,6,8,10]
    refute_equal(array, other_array)
  end

  def test_array_contains_same_number_of_elements_and_each_element_equal_to_coresponding_element_in_two_arrays
    array = [1,2,3, "cheese"]
    other_array = [1,2,3, "cheese"]
    assert_equal(array, other_array)
  end

  def test_array_element_reference_with_slice_and_range
    assert_equal([1,2,3], @array[0..2])
    assert_equal([4,5,6], @array[3, 3])
  end

  def test_array_at_returns_element_at_index
    assert_equal(@array.at(4), 5)
  end

  def test_array_clear_removes_all_elements_from_self
    array = [1,2,3,4,5,6, "cheetos", "mentos", "all sorts of find delectables"]
    assert_equal([], array.clear)
  end

  def test_array_collect_creates_a_new_array_containing_the_values_returned_by_the_block
    array = @array.collect { |x| x * 2  }
    assert_equal([2,4,6,8,10,12,14,16,18,20], array)
  end

  def test_array_combination__yeilds_all_combinations_of_length_n_elements_from_array
    expected_array = [
                      [1,2], [1,3], [1,4], [1,5], [1,6], [1,7], [1,8], [1,9], [1,10],
                      [2,3], [2,4], [2,5], [2,6], [2,7], [2,8], [2,9], [2,10],  
                      [3,4], [3,5], [3,6], [3,7], [3,8], [3,9], [3,10], 
                      [4,5], [4,6], [4,7], [4,8], [4,9], [4,10], 
                      [5,6], [5,7], [5,8], [5,9], [5,10], 
                      [6,7], [6,8], [6,9], [6,10], 
                      [7,8], [7,9], [7,10], 
                      [8,9], [8,10], 
                      [9,10]
                     ]

    actual_array = @array.combination(2).to_a
    assert_equal(expected_array, actual_array)
  end

  def test_array_compact_removes_nil_elements_from_the_array
    array = [1, nil, 2, nil, 3, nil, 4, nil, 5, nil]
    assert_equal([1,2,3,4,5], array.compact!)
  end

  def test_array_compact_returns_nil_if_no_changes_were_made
    assert_equal(nil, @array.compact!)
  end

  def test_array_concat_appends_elements_of_other_array_to_self
    first_array = [1,2,3]
    second_array = [4,5,6]
    assert_equal([1,2,3,4,5,6], first_array.concat(second_array))
  end

  def test_array_count_returns_number_of_elements_in_array
    test_array = [1,2,3,3,4,5,6]
    assert_equal(10, @array.count)
    assert_equal(2, test_array.count(3))
  end

  # def test_array_cycle_calls_given_block_4_times
  #   array = ["a", "b", "c"]
  #   # new_array = array.cycle(2) { |x| puts "#{x} " } 
  #   assert_equal(Enumerator: ["a", "b", "c"].cycle(2), array.cycle(2))
  # end

  def test_array_deletes_all_items_from_self_equal_to_obj
    array = [1,2,3,4,5,6,7,8,9,10]
    assert_equal(2, array.delete(2))
  end

  def test_array_delete_returns_block_results_if_item_is_not_found
    array = [1,2,3,4,5,6,7,8,9,10]
    assert_equal("fresh water fish", array.delete("salty?") { "fresh water fish" })
  end

  # alias to .slice!
  def test_array_delete_at_removes_element_at_specified_index_returning_that_element
    assert_equal(2, @array.delete_at(1))
    assert_equal(nil, @array.delete_at(101))
  end

  def test_array_delete_if_removes_every_element_of_self_which_the_block_evaluates_to_true
    scores = [ 97, 42, 75 ]
    result = scores.delete_if { |score| score < 80 }  
    assert_equal([97], result)
  end

  def test_array_drop_removes_the_first_5_elements_from_an_array_and_returns_the_rest_of_elements

  end
end
