require "minitest/spec"
require "minitest/autorun"
require "minitest/reporters"
Minitest::Reporters.use!

describe Array do
  let(:empty_array) { [] }
  let(:integers)    { [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }
  let(:strings)     { ["a", "b", "c", "d", "e", "f", "g"] }

  it "#new returns an empty array" do
    actual = empty_array
    expected = []
    assert_equal(expected, actual)
  end


  describe "adding arguments during array creation" do
    it "can be called with one argument" do
      actual = [nil, nil, nil]
      expected = Array.new(3)
      assert_equal(expected, actual)
    end

    it "can be called with two arguments" do
      actual = ['bob', 'bob', 'bob']
      expected = Array.new(3, 'bob')
      assert_equal(expected, actual)
    end
  end

  it "can contain different object types" do
    actual = [1, 'cat'.class]
    expected = String
    assert_equal(expected, actual[1])
  end

  describe "when accessing spcefic elements in an array" do
    it "accesses elements at a specific index with #at" do
      # Consider rewriting these with an array of strings
      actual = integers.at(2)
      expected = 3
      assert_equal(expected, actual)
    end

    it "accesses elemlents at a specific index with #fetch" do
      # Consider rewriting these with an array of strings
      actual = integers.fetch(1)
      expected = 2
      assert_equal(expected, actual)
    end

    it "returns the first element when using #first" do
      actual = strings.first
      expected = 'a'
      assert_equal(expected, actual)
    end

    it "will return the first n elements within itself using #take" do
      actual = integers.take(2)
      expected = [1,2]
      assert_equal(expected, actual)
    end

    it "will return remaining elements after n elements are removed with #drop" do
      actual = integers.drop(8)
      expected = [9, 10]
      assert_equal(expected, actual)
    end

    it "will count the number of elements using #length" do
      actual = integers.length
      expected = 10
      assert_equal(expected, actual)
    end

    it "will notify if array contains elements using #empty?" do
      actual = empty_array.empty?
      expected = true
      assert_equal(expected, actual)
    end

    it "will notify if arry contains specific item using #include?" do
      actual = strings.include?('d')
      expected = true
      assert_equal(expected, actual)
    end

    it "will append object to array using #<<" do
      actual = integers << 11
      expected = [1,2,3,4,5,6,7,8,9,10,11]
      assert_equal(expected, actual)
    end

    it "will prepend object to array using #unshift" do
      actual = integers.unshift(0)
      expected = [0,1,2,3,4,5,6,7,8,9,10]
      assert_equal(expected, actual)
    end

    it "will add an element at any position of array using #insert" do
      actual = strings.insert(1, 'cat')
      expected = ['a', 'cat', 'b', 'c', 'd', 'e', 'f', 'g']
      assert_equal(expected, actual)
    end

    it "will add multiple elements at one time using #insert" do
      actual = integers.insert(10, 11, 12, 13, 14, 15)
      expected = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
      assert_equal(expected, actual)
    end

    it " will remove last element from array and return it using #pop" do
      actual = integers.pop
      expected = 10
      assert_equal(expected, actual)
    end

    it "will retrieve and remove first element in array using #shift" do
      actual = integers.shift
      expected = 1
      assert_equal(expected, actual)
    end

    it "will delete any specific element in array using #delete_at" do
      actual = integers.delete_at(2)
      expected = 3
      assert_equal(expected, actual)
    end

    it "will remove all nil values at once using #compact" do

      nils = integers << nil << nil << nil
      actual = nils.compact
      expected = [1,2,3,4,5,6,7,8,9,10]
      assert_equal(expected, actual)
    end

    it "will remove duplicate elements in an array using #uniq but non-destructive" do
      array = integers << 1 << 2 << 3
      actual = array.uniq
      expected = [1,2,3,4,5,6,7,8,9,10]
      assert_equal(expected, actual)
    end

    it "will remove duplicate elemnets in an array using #uniq! wich permanently alters array" do
      integers = [1,1,1,2,3,2,3,3,3,2,2,2,2,1,3]
      expected = [1,2,3]
      actual = integers.uniq!
      assert_equal(expected, actual)
    end

    it "will iterate over array elements using #each" do
      expected = [1,2,3,4,5,6,7,8,9,10]
      actual = integers.each { |int| int }
      assert_equal(expected, actual)
    end

    it "will iterate over array elements in reverse order using #reverse_each" do
      expected = [10,9,8,7,6,5,4,3,2,1]
      actual = integers.reverse!
      assert_equal(expected, actual)
    end

    it "will create a new array based on an original array with #map" do
      expected = [2,4,6,8,10,12,14,16,18,20]
      actual = integers.map { |num| num * 2 }
      assert_equal(expected, actual)
    end

    it "will use #select to grab elements in array based on block criteria" do
      expected = [1,3,5,7,9]
      actual = integers.select { |num| num.odd? }
      assert_equal(expected, actual)
    end

    it "will remove elements from array while block conditions are true using #drop_while" do
      expected = [8,9,10]
      actual = integers.drop_while { |num| num < 8 }
      assert_equal(expected, actual)
    end

    it "will remove element from an array if block conditions are true using #delete_if" do
      expected = [6, 7, 8, 9, 10]
      actual = integers.delete_if { |num| num < 6 }
      assert_equal(expected, actual)
    end

    it "will keep elements in array if block conditions are true using #keep_if" do
      expected = [2, 4, 6, 8, 10]
      actual = integers.keep_if { |num| num % 2 == 0  }
      assert_equal(expected, actual)
    end
  end

  describe ".Array public class methods" do
    # Failing and unsure why, look to the top of Public Class Methods in Array docs
    # for more info.
    #
    # it "will return a new array using :size and :obj arguments" do
    #   # actual = Array.[](size:5, obj:"$$$")
    #   actual = Array.new(size:5, obj: "$$$")
    #   expected = ["$$$", "$$$", "$$$", "$$$", "$$$"]
    #   assert_equal(expected, actual)
    # end
    

    it "returns elements from first array when using first array as arg for .Array#new" do
      expected = ["Charles", "Barkley"]
      actual = Array.new(expected)
      assert_equal(expected, actual)
    end

    it "creates an array of the given size & elements are created by passing the index to a given block" do
      first_array = Array.new(3) { |index| index * 2 }
      expected = [0,2,4]
      actual = first_array
      assert_equal(expected, actual)
    end

    it "will create values for the array when seding a second parameter & same object is used for all values" do
      array = Array.new(2, 'rabbit')
      expected = ['rabbit', 'rabbit']
      actual = array
      assert_equal(expected, actual)
    end

    it "tries to convert object into an array and returns converted array or nil" do
      success_array = Array.try_convert([1,2,3])
      success_expected = [1,2,3]
      success_actual = success_array
      failure_array = Array.try_convert('1, 2, 3')
      failure_expected = nil
      failure_actual = failure_array
      assert_equal(success_expected, success_actual)
      assert_equal(failure_expected, failure_actual)
    end
  end

  describe ".Array public instance methods" do
    it "returns a new array containing elements common to two arrays excluding duplicates preserving order" do
      array = [1,1,3,5] & [1,2,3]
      expected = [1,3]
      actual = array
      assert_equal(expected, actual)
    end

    it "repeats with string argument or returns a new array build by concatenating the ing copies of itself" do
      repeat_array = [1,2,3] * 3
      repeat_expected = [1,2,3,1,2,3,1,2,3]
      repeat_actual = repeat_array
      concat_array = [1,2,3] * ","
      concat_expected = "1,2,3"
      concat_actual = concat_array
      assert_equal(repeat_expected, repeat_array)
      assert_equal(concat_expected, concat_array)
    end

    it "returns a new array build by concatenating two arrays together to produce a third array" do
      array = [1,2,3] + [4,5]
      expected = [1,2,3,4,5]
      actual = array
      assert_equal(expected, actual)
    end

    it "returns a new array as a copy of original, removing any items that also appear in other_ary, order is preserved" do
      first_array = [1,2,4]
      second_array = [1,1,2,2,3,3,4,5] - first_array
      expected = [3,3,5]
      actual = second_array
      assert_equal(expected, actual)
    end

    it "appends an object to the end of an array using #shovel" do
      expected = [1,2,3,4,5,6,7,8,9,10, "c", "d"]
      actual = integers << "c" << "d" 
      assert_equal(expected, actual)
    end

    it "returns an integer (-1, 0, or +1) if this array is less thanm equal to, or greater than other array" do
      array = [1,2,3,4]
      other_array = [1,2,3]
      expected = 1
      actual = array <=> other_array
      assert_equal(expected, actual)
    end

    it "returns boolean if two arrays contain same number of elements & if each element is equal to corresponding element in other array" do
      actual = integers
      expected = [1,2,3,4,5,6,7,8,9,10]
      assert_equal(expected, actual)
    end

    it "returns the element at index" do
      expected = 1
      actual = integers[0]
      assert_equal(expected, actual)
    end

    it "returns a subarray starting at the start index and continuing for length elements" do
      expected = ["b", "c", "d"]
      actual = strings[1, 3]
      assert_equal(expected, actual)
    end

    it "returns a subarray specified by range of indicies" do
      expected = [2,3,4]
      actual = integers[1..3]
      assert_equal(expected, actual)
    end

    it "sets element at index" do
      array = Array.new
      result = array[4] = "4"
      expected = [nil,nil,nil,nil,"4"]
      actual = array
      assert_equal(expected, actual)
    end

    it "replaces a subaray from the start index for length elements" do
      expected = ["a", "b", "c",4,5,6,7,8,9,10]
      integers[0,3] = ["a", "b", "c"]
      actual = integers
      assert_equal(expected, actual)
    end

    it "replaces a subarray specified by a range of indicies" do
      expected = ["a", 3, 4]
      integers[0..1] = "a"
      actual = integers[0..2]
      assert_equal(expected, actual)
    end

    # Returns nil instead of IndexError
    # 
    # it "returns IndexError if negative index points past beginning of array" do
    #   expected = nil
    #   actual = integers[22]
    #   assert_equal(expected, actual)
    # end

    it "returns first contained array that matches or nil if no matches found" do
      s1 = ["colors", "red", "blue", "green"]
      s2 = ["letters", "a", "b", "c"]
      s3 = "foo"
      array = [s1, s2, s3]
      letters_expected = ["letters", "a", "b", "c"]
      letters_actual = array.assoc("letters")
      foo_expected = nil
      foo_actual = array.assoc("foo")
      assert_equal(letters_expected, letters_actual)
      assert_equal(foo_expected, foo_actual)
    end

    it "returns the element at index, a negative index counts from the end of self and returns nil if index is out of range" do
      first_expected = "a"
      first_actual = strings.at(0)
      second_expected = 10
      second_actual = integers.at(9)
      third_expected = nil
      third_actual = integers.at(11)
      assert_equal(first_expected, first_actual)
      assert_equal(second_expected, second_actual)
      assert_equal(third_expected, third_actual)
    end

    it "finds a value from array which meets the contitions in a block" do
      expected = 1
      actual = integers.bsearch { |int| int >= -1 }
      assert_equal(expected, actual)
    end

    it "removes all elements from self" do
      expected = []
      actual = integers.clear
      assert_equal(expected, actual)
    end

    it "invokes the given block once for each element of self using #collect" do
      expected = ["a!", "b!", "c!", "d!", "e!","f!","g!"]
      actual = strings.collect { |x| x + "!" }
      assert_equal(expected, actual)
    end

    it "returns a copy of self with all nil elements removed" do
      array = [1, nil, 2, nil, 3, nil, 4, nil, 5, nil]
      expected = [1,2,3,4,5]
      actual = array.compact!
      assert_equal(expected, actual)
    end

    it "appends the elements of other_ary to self" do
      expected = [1,2,3,4]
      actual = [1,2].concat( [3,4] )
      assert_equal(expected, actual)
    end

    describe "when using #count" do
      it "returns the number of elements in array" do
        expected = 10
        actual = integers.count
        assert_equal(expected, actual)
      end

      it "counts the number of elements for which a block returns true" do
        expected = 5
        actual = integers.count { |int| int < 6 }
        assert_equal(expected, actual)
      end
    end

    describe "when using #cycle" do
      # it "calls the given block for each element n times" do
      #   expected = nil
      #   actual = integers.cycle(1) { |int| puts int }
      #   assert_equal(expected, actual)
      # end
    end

    describe "when using #delete" do
      it "deletes all items from self equal to object" do
        integers.delete(1)
        expected = [2,3,4,5,6,7,8,9,10]
        actual = integers
        assert_equal(expected, actual)
      end

      it "returns last deleted item or nil if no match is found" do
        expected = "g"
        actual = strings.delete("g")
        assert_equal(expected, actual)
      end
    end

    describe "when using #delete_at" do
      it "deletes the element at a the specified index, returning that element, or nil if out of range" do
        expected = 10
        actual = integers.delete_at(9)
        assert_equal(expected, actual)
      end

      it "is an alias of #slice!" do
        array = [1,2,3,4,5,6,7,8,9,10]
        actual = array.delete_at(9)
        expected = integers.slice!(9)
        assert_equal(expected, actual)
      end
    end

    describe "when using #delete_if" do 
      it "deletes every element of self where the block evaluates to true" do
        expected = [1, 3, 5, 7, 9]
        actual = integers.delete_if { |int| int % 2 == 0 }
        assert_equal(expected, actual)
      end

      it "is an alias of #reject!" do
        expected = integers.reject! { |int| int % 2 == 0 }
        actual = integers.delete_if { |int| int % 2 == 0 }
        assert_equal(expected, actual)
      end
    end

    describe "when using #drop" do
      it "drops the first n elements and returns the remainder elements in the array" do
        expected = [8,9,10]
        actual = integers.drop(7)
        assert_equal(expected, actual)
      end
    end

    describe "when using #drop_while" do
      it "drops elements up to but not including the first element where the block returns nil or false" do
        expected = [9, 10]
        actual = integers.drop_while { |num| num < 9 }
        assert_equal(expected, actual)
      end
    end

    describe "when using #each" do
      it "calls the given block once for each element in self" do
        expected = [1,2,3,4,5,6,7,8,9,10]
        actual = integers.each { |num| "#{num}, and " }
        assert_equal(expected, actual)
      end
    end

    # describe "when using #each_index" do
    #   it "passes the index of the element instead of the actual element" do
    #     expected = "0 -- 1 -- 2 --"
    #     actual = ["a", "b", "c"]
    #     actual.each_index {|x| print x, " -- " }
    #     assert_equal(expected, actual)
    #   end
    # end 

    describe "when using #empty?" do
      it "returns true if self contains no elements" do
        expected = true
        actual = [].empty?
        assert_equal(expected, actual)
      end 
    end

    describe "when using #eql" do
      it "returns true if self && other are the same object" do
        expected = true
        actual = 1.eql?(1)
        assert_equal(expected, actual)
      end

      it "returns true if both objects are arrays && contain the same content" do
        a = integers
        b = integers
        expected = true
        actual = a.eql?(b)
        assert_equal(expected, actual)
      end
    end

    describe "when using #fetch" do
      it "returns element at position index but throws IndexError if referenced index exceed array bounds" do
        expected = 3
        actual = integers.fetch(2)
        assert_equal(expected, actual)
      end
    end
  end
end 
